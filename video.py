#!/usr/bin/env python
#-*-coding:utf-8-*-

import sys
import cv2

if len(sys.argv) > 1:
	video_TP3 = sys.argv[1]

else:
	print(' Pass a filename as first argument ')
	sys.exit (0)

cap = cv2.VideoCapture(video_TP3)


#fourcc = cv2.VideoWriter_fourcc('X ','V','I','D')
fourcc = cv2.VideoWriter_fourcc(*'XVID')

#framesize = (640,480)

# Obtiene el ancho y alto del video de entrada
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

framesize = (width, height)

print('ancho es ',width)
print('largo es ',height)

# Obtiene el fps del video
fps = cap.get(cv2.CAP_PROP_FPS)
delay = int(1000 / fps)

print('delay es ',delay)

#fps = 20.0

out = cv2.VideoWriter('output.avi', fourcc, fps , framesize)

#delay = 33

while (cap.isOpened()):

	ret,frame = cap.read()
	if ret is True :
		gray = cv2.cvtColor (frame, cv2.COLOR_BGR2GRAY)
		out.write(gray)
		cv2.imshow('Image gray',gray)
		if cv2.waitKey(delay) & 0xFF == ord ('q'):
			break

	else:
		break

cap.release()
out.release()
cv2.destroyAllWindows() 
