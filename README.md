# Práctico 3 Propiedades de video

[Propiedades de video](https://gitlab.com/vision-por-computadora615916/practico-3-propiedades-de-video/-/blob/main/video.py?ref_type=heads) es un proyecto en Python que trabaja con la captura de videos.

Este script en Python utiliza OpenCV para procesar un video, convirtiendo cada cuadro a escala de grises y guardando el video resultante en un nuevo archivo `output.avi`.

## Funcionalidad:

- Lectura de Argumentos: Verifica y lee el nombre del archivo de video proporcionado como argumento.

- Captura de Video: Abre el video de entrada y obtiene sus propiedades (ancho, alto, fps).

- Configuración: Prepara el codificador de video XVID y configura el VideoWriter.

- Procesamiento: Convierte cada cuadro a escala de grises, lo guarda en el archivo de salida y lo muestra en una ventana.
